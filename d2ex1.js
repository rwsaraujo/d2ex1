// Declaração do vetor
var coisa = Array(10);
var aux;

// Cada posição do vetor recebe um número aleatório entre 1 e 100
for(var cont = 0; cont < 10; cont++) {
    coisa[cont] = Math.floor(Math.random() * 100);
}

// Ordenação do vetor
for(var contX = 0; contX < 10; contX++) {
    for(var contY = 0; contY < 10; contY++) {
        if(coisa[contX] < coisa[contY]) {
            aux = coisa[contX];
            coisa[contX] = coisa[contY];
            coisa[contY] = aux;
        }
    }
}

// Imprime no console o valor de cada posição do vetor ordenado
coisa.forEach(element => console.log(element));